import React, {memo, useContext} from 'react';
import classNames from 'classnames';
import {NavLink} from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Paper, Grid, Toolbar, Tooltip, Typography, Button } from '@material-ui/core';

import {navbarContext} from '../../../contexts';

const styles = theme => ({
  root: {
    flexGrow: 1,
    background: '#f2f2f2',
    height: '91vh',
    width: '100%',
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageBackground: {
    height: '60%',
    width: '100%',
    zIndex: 1,
    position: 'absolute',
    top: 0
  },
  about: {
    background: '#fff',
    zIndex: 5,
    position: 'absolute',
    width: '70%',
    height: 'auto',
    padding: '45px 25px',
    borderRadius: '3px',
    margin: 0,
    boxShadow: '0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)',
    [theme.breakpoints.down('sm')]: {
      marginTop: 250,
      width: '90%'
    },
    [theme.breakpoints.down('xs')]: {
      marginTop: 200,
      width: '90%'
    }

  },
  paperLeft: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    background: '#fff',
    boxShadow: 'none'
  },
  paperRight: {
    display: 'flex',
    flexDirection: 'column',
    background: '#fff',
    boxShadow: 'none',
    [theme.breakpoints.down('xs')]: {
      alignItems: 'center'
    }
  },
  avatar: {
    width: 230,
    height: 230,
    borderRadius: "100%",
    marginTop: 30,
    marginBottom: 30,
    [theme.breakpoints.down('xs')]: {
      width: 180,
      height: 180,
    }
  },
  social: {
    margin: '0px 7px',
    color: '#969696'
  },
  fullname: {
    fontSize: '35px',
    lineHeight: '35px',
    color: '#444',
    marginTop: 30,
    fontWeight: 500
  },
  job: {
    color: '#848484',
    fontSize: '20px',
    lineHeight: '25px',
    margin: '30px 0px',
    textTransform: 'uppercase',
    fontWeight: 500
  },
  buttonLink: {
    textDecoration: 'none'
  },
  buttonDownload: {
    marginRight: '10px',
    color: '#fff',
    background: '#00cf6b',
    fontWeight: 500,
  },
  buttonContact: {
    color: '#fff',
    background: '#2e8dfa',
    fontWeight: 500
  },
  info: {
    margin: "30px 0px",
    fontSize: '18px',
    lineHeight: '25px',
  },
  infoRowContentLeft: {
    color: '#444',
    paddingRight: '20px'
  },
  infoRowContentRight: {
    color: '#969696',
    paddingLeft: '20px',
    fontWeight: 600
  }
});

const AboutScreen = (props) => {
  const {classes} = props;
  const {onActive} = useContext(navbarContext);

  return (
    <React.Fragment>
      <CssBaseline>
        <Paper className = {classes.root}>
          <img 
            alt = "background"
            src = "/asserts/images/img_bg.jpg"
            className = {classes.imageBackground}
          />
          <Grid 
            container 
            spacing = {24} 
            className = {classes.about}
          >
            <Grid item 
              xs = {12}
              sm = {12}
              md = {5}
            >
              <Paper className={classNames(classes.paperLeft)}>
                <img 
                  alt = "avatar"
                  src = "/asserts/images/img_avatar.jpg"
                  className = {classes.avatar}
                />
                <Toolbar >
                  <Tooltip title = "Facebook" placement = "bottom">
                      <a href = "https://fb.com/nqa97" className = {classes.social} target = "_blank" rel='noopener noreferrer' >
                          <i className="fab fa-facebook-f"></i>
                      </a>
                  </Tooltip>
                  <Tooltip title = "Email" placement = "bottom">
                      <a href = "mailto:ngquangan@gmail.com" className = {classes.social} target = "_blank" rel='noopener noreferrer' >
                          <i className="fas fa-envelope"></i>
                      </a>
                  </Tooltip>
                  <Tooltip title = "Linkedin" placement = "bottom">
                      <a href = "https://linkedin.com/in/ngquangan" className = {classes.social} target = "_blank" rel='noopener noreferrer' >
                          <i className="fab fa-linkedin"></i>
                      </a>
                  </Tooltip>
                  <Tooltip title = "Github" placement = "bottom">
                      <a href = "https://github.com/ngquangan" className = {classes.social} target = "_blank" rel='noopener noreferrer' >
                          <i className="fab fa-github"></i>
                      </a>
                  </Tooltip>
                  <Tooltip title = "Gitlab" placement = "bottom">
                      <a href = "https://gitlab.com/ngquangan" className = {classes.social} target = "_blank" rel='noopener noreferrer' >
                          <i className="fab fa-gitlab"></i>
                      </a>
                  </Tooltip>
                </Toolbar>
              </Paper>
            </Grid>
            <Grid item
              xs = {12} 
              sm = {12} 
              md = {7}
            >
              <Paper className={classes.paperRight}>
                <Typography variant = "h1" className={classes.fullname}>
                  Nguyễn Quang An
                </Typography>
                <Typography variant = "h5" className = {classes.job}>
                  Fullstack Javascript Developer
                </Typography>
                <div>
                  <a 
                    href = "https://firebasestorage.googleapis.com/v0/b/blog-nguyenquangan.appspot.com/o/cv%2Fcv-ngquangan.pdf?alt=media&token=3c9ce9f3-09e0-4224-9dd1-bb18a8c3f9c1" 
                    className={classes.buttonLink}
                  >
                    <Button variant="contained" color="primary" className={classes.buttonDownload}>
                      Download CV
                    </Button>
                  </a>
                  <NavLink to = "/contact" className = {classes.buttonLink}>
                    <Button 
                      variant="contained" 
                      color="secondary" 
                      className={classes.buttonContact}
                      onClick = {() => onActive(6)}
                    >
                      Contact
                    </Button>
                  </NavLink>

                </div>
                <table className = {classes.info}>
                  <tbody>
                    <tr>
                      <td className = {classes.infoRowContentLeft}>Age</td>
                      <td className = {classes.infoRowContentRight}>21</td>
                    </tr>
                    <tr>
                      <td className = {classes.infoRowContentLeft}>Address</td>
                      <td className = {classes.infoRowContentRight}>Da Nang, Viet Nam</td>
                    </tr>
                    <tr>
                      <td className = {classes.infoRowContentLeft}>Email</td>
                      <td className = {classes.infoRowContentRight}>ngquangan@gmail.com</td>
                    </tr>
                    <tr>
                      <td className = {classes.infoRowContentLeft}>Phone</td>
                      <td className = {classes.infoRowContentRight}>0961314293</td>
                    </tr>
                  </tbody>
                  </table>
              </Paper>
            </Grid>
          </Grid>
        </Paper>
      </CssBaseline>
    </React.Fragment>
  )
}

export default withStyles(styles)(memo(AboutScreen));

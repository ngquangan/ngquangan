import React, {memo} from 'react'
import {Switch, Route, withRouter} from 'react-router-dom';

//import screen
import Navbar from '../../../commons/Navbar/Navbar';
import AboutScreen from '../../About/components/AboutScreen';
import SkillScreen from '../../Skill/components/SkillScreen';
import ProjectScreen from '../../Project/components/ProjectScreen';
import EducationScreen from '../../Education/components/EducationScreen';
import BlogScreen from '../../Blog/components/BlogScreen';
import ContactScreen from '../../Contact/components/ContactScreen';
import ErrorPageScreen from '../../ErrorPage/components/ErrorPageScreen';
import DrawerComponent from '../../../commons/Drawer/DrawerComponent';

//import context
import {drawerContext, navbarContext} from '../../../contexts';

//import hooks
import {drawerHook, navbarHook} from '../../../hooks';

const Initial_Menu = [
    {
        id: 1,
        to: '/about',
        name: 'About',
        icon: 'fas fa-address-card'
    },
    {
        id: 2,
        to: '/skills',
        name: 'Skills',
        icon: 'fas fa-code'
    },
    {
        id: 3,
        to: '/project',
        name: 'Project',
        icon: 'fas fa-project-diagram'
    },
    {
        id: 4,
        to: '/education',
        name: 'Education',
        icon: 'fas fa-graduation-cap'
    },
    {
        id: 5,
        to: '/blog',
        name: 'Blog',
        icon: 'fas fa-blog'
    },
    {
        id: 6,
        to: '/contact',
        name: 'Contact',
        icon: 'fas fa-envelope'
    }
]


const HomeScreen = (props) => {

    const {isOpen, onToggle} = drawerHook();
    const {menus, menuActive, onActive} = navbarHook(Initial_Menu);

    const drawerValue = {
        isOpen,
        onToggle,
    }

    const navbarValue = {
        menus,
        menuActive,
        onActive
    }

    return (
            <React.Fragment>
                <drawerContext.Provider value = {drawerValue}>
                    <navbarContext.Provider value = {navbarValue}>
                        <Navbar />
                        <DrawerComponent />
                        <Switch>
                            <Route 
                                path = "/"
                                component = {AboutScreen}
                                exact
                            />
                            <Route 
                                path = "/about"
                                component = {AboutScreen}
                            />
                            <Route 
                                path = "/skills"
                                component = {SkillScreen}
                            />
                            <Route 
                                path = "/project"
                                component = {ProjectScreen}
                            />
                            <Route 
                                path = "/education"
                                component = {EducationScreen}
                            />
                            <Route 
                                path = "/blog"
                                component = {BlogScreen}
                            />
                            <Route 
                                path = "/contact"
                                component = {ContactScreen}
                            />
                            <Route 
                                component = {ErrorPageScreen}
                            />
                        </Switch>
                    </navbarContext.Provider>
                </drawerContext.Provider>
            </React.Fragment>
    );
}

export default withRouter(memo(HomeScreen));
import {createContext} from 'react';

const drawer = createContext();

export default drawer;
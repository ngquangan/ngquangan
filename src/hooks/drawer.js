import {useState} from 'react';

const useDrawer = () => {
    const [isOpen, setIsOpen] = useState(false);

    return {
        isOpen,
        onToggle: () => {
            setIsOpen(!isOpen);
        }
    }
}

export default useDrawer;
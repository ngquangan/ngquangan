import {useState} from 'react';

const useNavbar = (Initial_Menu = []) => {
    const [menuActive, setMenuActive] = useState(1);
    const [menus] = useState(Initial_Menu);

    return {
        menuActive,
        menus,
        onActive: (pos) => {
            setMenuActive(pos);
        }
    }
}

export default useNavbar;
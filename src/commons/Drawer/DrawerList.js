import React, {useContext, memo} from 'react';
import classNames from 'classnames'
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { List, ListItem , ListItemIcon, ListItemText} from '@material-ui/core';

import {navbarContext} from '../../contexts';

const styles = {
    drawerList: {
        backgroundColor: '#272727',
        paddingTop: 0
    },
    listItem: {
        borderTop: '1px solid rgba(0, 0, 0, 0.1)',
        paddingTop: 16,
        paddingBottom: 16,
        paddingLeft: 40,
        paddingRight: 40,
    },
    listItemContent: {
        color: '#969696',
        textDecoration: 'none',
        fontWeight: 500
    },
    listItemActive: {
        backgroundColor: '#272727'
    },
    listItemContentActive: {
        color: '#fff',
        fontWeight: 700,
    },
    listItemEnd: {
        height: '100px'
    }
  };

const DrawerList = (props) => {
    const { classes } = props;
    const {menus, menuActive, onActive} = useContext(navbarContext);
    return (
        <React.Fragment>
            <CssBaseline>
                <List className = {classes.drawerList}>
                    {
                        menus.map((menu,index) => {
                            return (
                                <NavLink to = {menu.to} className = {classes.listItemContent} key = {index}>
                                    <ListItem button 
                                        className = {classNames(classes.listItem, menu.id === menuActive ? classes.listItemActive : "")}
                                        onClick = {() => onActive(menu.id)}
                                    >
                                        <ListItemIcon className = {classNames(classes.listItemContent, menu.id === menuActive ? classes.listItemContentActive : "")}>
                                            <i className = {menu.icon}></i>
                                        </ListItemIcon>
                                        <ListItemText>
                                            <span className = {classNames(classes.listItemContent, menu.id === menuActive ? classes.listItemContentActive : "")}>{menu.name}</span>
                                        </ListItemText>
                                    </ListItem>
                                </NavLink>
                            );
                        })
                    }
                    <ListItem 
                        className = {classNames(classes.listItem, classes.listItemEnd)}
                    >
                    </ListItem>
                </List>
            </CssBaseline>
        </React.Fragment>
    );
  }
  

  DrawerList.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(memo(DrawerList));

import React, {memo} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import { Typography, Toolbar, Tooltip } from '@material-ui/core';

const styles = {
    paperInfo: {
        width: 'auto',
        height: 'auto',
        backgroundColor: '#272727',
        textAlign: 'center',
    },
    avatar: {
        width: 123,
        height: 123,
        borderRadius: 123,
        marginTop: 25,
        marginBottom: 20
    },
    name: {
        color: '#fff',
        fontSize: 18,
    },
    listSocial: {
        padding: 0,
        display: 'flex',
        justifyContent: 'center'
    },
    social: {
        color: '#848484',
        marginLeft: 7,
        marginRight: 7
    }

  };

const DrawerInfo = (props) => {
    const { classes } = props;

    return (
        <React.Fragment>
            <CssBaseline>
                <Paper className = {classes.paperInfo} square = {true}>
                    <img 
                        src = "/asserts/images/img_avatar.jpg"
                        alt = "avatar"
                        className = {classes.avatar}
                    />
                    <Typography variant = "h6" className = {classes.name}>
                        Nguyễn Quang An
                    </Typography>
                    <Toolbar className = {classes.listSocial}>
                        <Tooltip title = "Facebook" placement = "bottom">
                            <a href = "https://fb.com/nqa97" className = {classes.social} target = "_blank" rel='noopener noreferrer' >
                                <i className="fab fa-facebook-f"></i>
                            </a>
                        </Tooltip>
                        <Tooltip title = "Email" placement = "bottom">
                            <a href = "mailto:ngquangan@gmail.com" className = {classes.social} target = "_blank" rel='noopener noreferrer' >
                                <i className="fas fa-envelope"></i>
                            </a>
                        </Tooltip>
                        <Tooltip title = "Linkedin" placement = "bottom">
                            <a href = "https://linkedin.com/in/ngquangan" className = {classes.social} target = "_blank" rel='noopener noreferrer' >
                                <i className="fab fa-linkedin"></i>
                            </a>
                        </Tooltip>
                        <Tooltip title = "Github" placement = "bottom">
                            <a href = "https://github.com/ngquangan" className = {classes.social} target = "_blank" rel='noopener noreferrer' >
                                <i className="fab fa-github"></i>
                            </a>
                        </Tooltip>
                        <Tooltip title = "Gitlab" placement = "bottom">
                            <a href = "https://gitlab.com/ngquangan" className = {classes.social} target = "_blank" rel='noopener noreferrer' >
                                <i className="fab fa-gitlab"></i>
                            </a>
                        </Tooltip>
                    </Toolbar>
                </Paper>
            </CssBaseline>
        </React.Fragment>
    );
  }
  

DrawerInfo.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(memo(DrawerInfo));

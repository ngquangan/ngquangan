import React, {useContext, memo} from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Drawer } from '@material-ui/core';

import {drawerContext} from '../../contexts';
import DrawerInfo from './DrawerInfo';
import DrawerList from './DrawerList';


const styles = {
    drawerMenu: {
        width: 260,
        backgroundColor: '#2b2b2b',
    },
  };

const DrawerComponent = (props) => {
    const { classes } = props;
    const {isOpen, onToggle} = useContext(drawerContext);

    return (
        <React.Fragment>
            <CssBaseline>
                <Drawer open={isOpen} onClose={() => onToggle()}>
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={() => onToggle()}
                        onKeyDown={() => onToggle()}
                    >
                        <div className={classes.drawerMenu}>
                            <DrawerInfo />
                            <DrawerList />
                        </div>
                    </div>
                </Drawer>
            </CssBaseline>
        </React.Fragment>

    );
  }
  

DrawerComponent.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(memo(DrawerComponent));

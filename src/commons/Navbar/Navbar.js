import React, {useContext, memo} from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import classNames from 'classnames';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import CssBaseline from '@material-ui/core/CssBaseline';
import Hidden from '@material-ui/core/Hidden';

import {drawerContext, navbarContext} from '../../contexts';

const styles = {
    root: {
      flexGrow: 1,
    },
    logo: {
      flexGrow: 1,
      color: '#000',
      textDecoration: 'none'
    },
    menu: {
      marginLeft: -12,
      marginRight: 20,
    },
    navbar: {
        backgroundColor: '#fff'
    },
    menuItem: {

        textDecoration: 'none',

    },
    menuItemContent: {
        fontSize: 14,
        color: '#444',
        textTransform: 'capitalize',
        fontWeight: 400,
        '&:hover' : {
            color: '#02c853',
            backgroundColor:'#fff'
        }
    },
    menuItemActive: {
        backgroundColor: '#02c853',
        color: '#fff',
        fontWeight: '700'
    }

  };

const Navbar = (props) => {
    const { classes } = props;
    const {onToggle} = useContext(drawerContext);
    const {onActive, menuActive, menus} = useContext(navbarContext);

    return (
        <React.Fragment>
            <CssBaseline>
                <div className={classes.root}>
                    <AppBar position="static" className = {classes.navbar}>
                        <Toolbar >
                            <IconButton 
                                className={classes.menu} 
                                aria-label="Menu"
                                onClick = {() => onToggle()}    
                            >
                                <MenuIcon />
                            </IconButton>
                            <Typography variant="h6" className={classes.logo}>
                                ngquangan
                            </Typography>
                            <Hidden
                                smDown = {true}
                            >
                                {
                                    menus.map((menu, index) => {
                                        return (
                                            <NavLink 
                                                to = {menu.to} 
                                                className = {classes.menuItem}
                                                key = {index}
                                            >
                                                <Button 
                                                    className = {classNames(classes.menuItemContent, menu.id === menuActive ? classes.menuItemActive : "")} 
                                                    onClick = {() => onActive(menu.id)}
                                                >{menu.name}</Button>
                                            </NavLink>
                                        )
                                    })
                                }
                            </Hidden>
                        </Toolbar>
                    </AppBar>
                </div>
            </CssBaseline>
        </React.Fragment>

    );
  }
  

Navbar.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(memo(Navbar));
